package com.ds;

import org.apache.james.mime4j.MimeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class MainTest {
    @Test
    @DisplayName("Проверка на отсутсвие исключений")
    public void doestThrowTest() {
        Assertions.assertDoesNotThrow(() -> Main.main(new String[]{}));
    }

    @Test
    @DisplayName("Проверка корректности парсинга писем")
    public void notEmptyMailTest() throws IOException, MimeException {
        Main.main(new String[]{});
        Assertions.assertFalse(Main.getMails().isEmpty());
        Assertions.assertEquals(Main.getMails().size(), 1797);
    }

    @Test
    @DisplayName("Проверка корректности составления диаграммы автор-количество писем")
    public void notEmptyAuthorMailCount() throws IOException, MimeException {
        Main.main(new String[]{});
        Assertions.assertFalse(Main.getHashMap().isEmpty());
    }

    @Test
    @DisplayName("Проверка получения DSPAM Probability")
    public void notBlankDSPAMProbability() throws IOException, MimeException {
        Main.main(new String[]{});
        Assertions.assertNotNull(Main.getAverageValue());
    }

    @Test
    @DisplayName("Проверка диаграммы PieChart")
    public void notNullPieChart() throws IOException, MimeException {
        Main.main(new String[]{});
        Assertions.assertNotNull(Main.getChart());
    }
}
