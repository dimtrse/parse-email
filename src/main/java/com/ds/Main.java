package com.ds;

import com.ds.chart.FilePieChart;
import com.ds.model.Mail;
import org.apache.commons.io.FileUtils;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.mboxiterator.CharBufferWrapper;
import org.apache.james.mime4j.mboxiterator.MboxIterator;
import org.apache.james.mime4j.parser.MimeStreamParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    private static List<Mail> mails = new ArrayList<>();
    private static Map<String, Integer> hashMap;
    private static double averageValue;
    private static FilePieChart chart;

    public static void main(String[] args) throws IOException, MimeException {

        final CharsetEncoder ENCODER = StandardCharsets.UTF_8.newEncoder();
        URL url = new URL("https://www.pythonlearn.com/code3/mbox.txt");
        URLConnection urlConnection = url.openConnection();
        urlConnection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
        InputStream input = urlConnection.getInputStream();

        File file = File.createTempFile("temfile", ".bin");
        file.deleteOnExit();
        FileUtils.copyInputStreamToFile(input, file);

        mails = new ArrayList<>();
        for (final CharBufferWrapper message : MboxIterator.fromFile(file).charset(ENCODER.charset()).build()) {
            Mail mail = new Mail();
            MimeStreamParser parser = new MimeStreamParser();
            parser.setContentHandler(new CustomContentHandler(mail));
            parser.parse(message.asInputStream(ENCODER.charset()));
            mails.add(mail);
        }

        double sum = 0.00;
        for (Mail mail : mails) sum += mail.getxDSPAMProbability();
        averageValue = sum / mails.size();
        System.out.println("Average value X-DSPAM-Probability: " + averageValue);

        hashMap = new HashMap<>();
        for (Mail mail : mails) {
            if (!hashMap.containsKey(mail.getFrom())) hashMap.put(mail.getFrom(), 1);
            else hashMap.replace(mail.getFrom(), hashMap.get(mail.getFrom()) + 1);
        }

        chart = new FilePieChart("Senders", hashMap);
        // or BarChart chart = new BarChart("Senders", hashMap);
        chart.saveToFile();
//        UIUtils.centerFrameOnScreen(chart);
//        chart.setVisible(true);
    }

    public static List<Mail> getMails() {
        return mails;
    }

    public static Map<String, Integer> getHashMap() {
        return hashMap;
    }

    public static double getAverageValue() {
        return averageValue;
    }

    public static FilePieChart getChart() {
        return chart;
    }
}