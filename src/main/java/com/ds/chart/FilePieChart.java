package com.ds.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class FilePieChart {
    private final JFreeChart chart;

    public FilePieChart(String title, Map<String, Integer> data) {
        chart = ChartFactory.createPieChart(
                title,   // chart title
                createDataset(data),
                false,
                false,
                false
        );
    }

    public void saveToFile() throws IOException {
        ChartUtils.saveChartAsPNG(new File("pie.png"), chart, 500, 500);
    }

    public PieDataset createDataset(Map<String, Integer> data) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        data.forEach(dataset::setValue);
        return dataset;
    }
}
